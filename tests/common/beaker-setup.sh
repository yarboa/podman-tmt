#!/bin/bash

set -uxo pipefail

sed -i 's/$stream/9-stream/' /etc/yum.repos.d/*
sed -i 's/arch/basearch/' /etc/yum.repos.d/epel.repo
sed -i 's/os\///' /etc/yum.repos.d/epel.repo
if [ ! -f "/etc/yum.repos.d/beaker-client.repo" ]; then
  wget -O /etc/yum.repos.d/beaker-client.repo https://beaker-project.org/yum/beaker-client-Fedora.repo
  sed -i 's/\$releasever/34/' /etc/yum.repos.d/beaker-client.repo
  rpm-ostree install -A beaker-client
fi

if [ ! -f "/etc/yum.repos.d/beaker-harness.repo" ]; then
  wget -O /etc/yum.repos.d/beaker-harness.repo https://beaker-project.org/yum/beaker-harness-Fedora.repo
  sed -i 's/\$releasever/34/' /etc/yum.repos.d/beaker-harness.repo
  rpm-ostree install -A restraint -A restraint-client
fi
