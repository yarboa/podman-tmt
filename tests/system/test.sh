#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
. /usr/share/beakerlib/beakerlib.sh || exit 1

rlJournalStart
    rlPhaseStartSetup
        rlRun "tmp=\$(mktemp -d)" 0 "Create tmp directory"
        # rlRun "pushd $tmp"
        rlRun "cd $tmp"
        rlRun "set -o pipefail"
    rlPhaseEnd

    rlPhaseStartTest
        #rlRun "tmt --help | tee output" 0 "Check help message"
        rlRun "systemctl is-active sshd" 0 "Check message status"
        #rlAssertGrep "active" "output"
    rlPhaseEnd

    rlPhaseStartCleanup
        # rlRun "popd"
        rlRun "cd -"
        rlRun "rm -r $tmp" 0 "Remove tmp directory"
    rlPhaseEnd
rlJournalEnd
